#include "qhttpserverfwd.h"

#include <QObject>

/// Greeting

class Foldercontent : public QObject
{
    Q_OBJECT

public:
    Foldercontent();

private slots:
    void handleRequest(QHttpRequest *req, QHttpResponse *resp);
};
