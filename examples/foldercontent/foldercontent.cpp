#include "foldercontent.h"

#include <QCoreApplication>
#include <QRegExp>
#include <QStringList>

#include <qhttpserver.h>
#include <qhttprequest.h>
#include <qhttpresponse.h>

#include <QDir>
#include <QtCore>
#include <iostream>

Foldercontent::Foldercontent()
{
    QHttpServer *server = new QHttpServer(this);
    connect(server, SIGNAL(newRequest(QHttpRequest*, QHttpResponse*)),
            this, SLOT(handleRequest(QHttpRequest*, QHttpResponse*)));

    server->listen(QHostAddress::Any, 8080);
}

void Foldercontent::handleRequest(QHttpRequest *req, QHttpResponse *resp)
{

    QRegExp exp("^/filelist$");
    if( exp.indexIn(req->path()) != -1 )
    {
        QDir dir;  //объявляем объект работы с папками
        dir.setFilter(QDir::Files | QDir::Hidden | QDir::NoSymLinks);
        QStringList list = dir.entryList();
        QString answer;
        const QString mySeparator = QString("</p><p>");
        answer= list.join(mySeparator);

        resp->setHeader("Content-Type", "text/html");
        resp->writeHead(200);
        QString body = "<html><head><title>Folder content App</title></head><body><h1>File list in dir:</h1> <p>%1!</p></body></html>";
        resp->end(body.arg(answer).toUtf8());
        return;
    }
    exp.setPattern("^/file/.*$");
    if( exp.indexIn(req->path()) != -1 )
    {
        QFile file;
        QString answer;
        QString name = exp.capturedTexts()[0];
        name.remove(0,6);
        qDebug()<<name;
        file.setFileName(name);
        if(!file.exists())        {
            answer="not such file: "+name;
        }
        else{
            if (!file.open(QIODevice::ReadOnly))            {
                qDebug() << "Ошибка открытия для чтения";
            }
            else {
                QByteArray a = file.readAll();
                answer=QString(a);
                file.close();
            }
        }

        resp->setHeader("Content-Type", "text/html");
        resp->writeHead(200);
        QString body = "<html><head><title>Folder content App</title></head><body><h1>File list in dir:</h1> <p>%1!</p></body></html>";
        resp->end(body.arg(answer).toUtf8());
        return;
    }
    resp->writeHead(403);
    resp->end(QByteArray("I dont know what to do"));

}

/// main

int main(int argc, char **argv)
{
    QCoreApplication app(argc, argv);
    Foldercontent f;
    app.exec();
}
